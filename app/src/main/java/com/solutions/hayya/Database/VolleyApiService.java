package com.solutions.hayya.Database;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.solutions.hayya.Utils.ErrorManager;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class VolleyApiService {

    Context context;
    ErrorManager errorManager;
    private String BASE_URL = "http://hayyaworld.com/HayyaApp/";

    //private String BASE_URL = "http://10.0.2.2/Hayya/";

    public VolleyApiService(Context context_, ErrorManager errorManager_) {
        errorManager = errorManager_;
        context=context_;
    }

    public void Volley_GET(String url, final VolleyCallback callback)
    {

        StringRequest strREQ = new StringRequest(Request.Method.GET, BASE_URL+url, new Response.Listener < String > ()
        {
            @Override
            public void onResponse(String Response)
            {
                callback.onSuccessResponse(Response);
            }

        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                handleVolleyError(error);
                callback.onErrorResponse();
            }
        })
        {
            /**
             * Passing some request headers
             * */
/*            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                return createBasicAuthHeader("enter_username", "enter_password");
            }*/

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "charset=utf-8");
                return params;
            }

            @Override
            public void deliverError(VolleyError error) {
                if (error instanceof NoConnectionError) {
                    Cache.Entry entry = this.getCacheEntry();
                    if (entry != null) {
                        Response<String> response = parseNetworkResponse(new NetworkResponse(entry.data, entry.responseHeaders));
                        deliverResponse(response.result);
                        return;
                    }
                }
                super.deliverError(error);
            }
        };
        MySingleton.getInstance(context).addToRequestQueue(strREQ);
    }
    private HashMap<String, String> createBasicAuthHeader(String username, String password)
    {
        HashMap<String, String> headerMap = new HashMap<String, String>();

        String credentials = username + ":" + password;
        String base64EncodedCredentials =
                Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        headerMap.put("Authorization", "Basic " + base64EncodedCredentials);

        return headerMap;
    }
    String BOUNDARY = "HAYAA";

    public void Volley_POST(final Map<String, String> params, String url, final VolleyCallback callback)
    {
        try
        {
            final RequestQueue requestQueue = Volley.newRequestQueue(context);

            final StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + url , new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    callback.onSuccessResponse(response);

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    handleVolleyError(error);
                    callback.onErrorResponse();
                }

            })
            {
                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type", "multipart/form-data; boundary=" + BOUNDARY+"; charset=utf-8");
                    return params;
                }

                @Override
                public String getBodyContentType() {
                    return "multipart/form-data; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        String postBody = createPostBody(params);

                        return postBody == null ? null : postBody.getBytes("utf-8");
                    }
                    catch (NegativeArraySizeException n)
                    {
                        n.printStackTrace();
                        return null;
                    }
                    catch (UnsupportedEncodingException uee)
                    {

                        uee.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response)
                {

                    String responseString = "post_error";
                    if (response != null)
                    {
                        try
                        {
                            responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
                @Override
                public void deliverError(VolleyError error) {
                    if (error instanceof NoConnectionError) {
                        Cache.Entry entry = requestQueue.getCache().get(this.getCacheKey());
                        if (entry != null) {
                            Response<String> response = parseNetworkResponse(new NetworkResponse(entry.data, entry.responseHeaders));
                            deliverResponse(response.result);
                            return;
                        }
                    }
                    super.deliverError(error);
                }

                @Override
                public String getCacheKey() {
                    return generateCacheKeyWithParam(super.getCacheKey(), params);
                }
            };

            //stringRequest.setRetryPolicy(new DefaultRetryPolicy( 50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (NegativeArraySizeException n)
        {
            n.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



    }
    private String createPostBody(Map<String, String> params) {
        StringBuilder sbPost = new StringBuilder();
        for (String key : params.keySet()) {
            if (params.get(key) != null) {
                sbPost.append("\r\n" + "--" + BOUNDARY + "\r\n");
                sbPost.append("Content-Disposition: form-data; name=\"" + key + "\"" + "\r\n\r\n");
                sbPost.append(params.get(key));
            }
        }

        return sbPost.toString();
    }

    private void handleVolleyError(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            errorManager.setCode(8);
        } else if (error instanceof AuthFailureError) {
            //Error indicating that there was an Authentication Failure while performing the request
            errorManager.setCode(9);
        } else if (error instanceof ServerError) {
            //Indicates that the server responded with a error response
            errorManager.setCode(10);
        } else if (error instanceof NetworkError) {
            //Indicates that there was network error while performing the request
            errorManager.setCode(11);
        } else if (error instanceof ParseError) {
            // Indicates that the server response could not be parsed
            errorManager.setCode(12);
        }
    }

    private static String generateCacheKeyWithParam(String url, Map<String, String> params) {
        StringBuilder urlBuilder = new StringBuilder(url);
        for (Map.Entry<String, String> entry : params.entrySet()) {
            urlBuilder.append(entry.getKey()).append("=").append(entry.getValue());
        }
        url = urlBuilder.toString();
        return url;
    }
}