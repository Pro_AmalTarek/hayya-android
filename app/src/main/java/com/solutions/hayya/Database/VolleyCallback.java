package com.solutions.hayya.Database;

public interface VolleyCallback {

    void onSuccessResponse(String result);
    void onErrorResponse();
}
