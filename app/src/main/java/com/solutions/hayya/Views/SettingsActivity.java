package com.solutions.hayya.Views;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.solutions.hayya.Utils.SessionManager;
import com.solutions.hayya.R;
import com.solutions.hayya.Utils.SoundManager;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

public class SettingsActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private TextView name, email;
    private Button btn_logout;
    private String mType, mPhone = "", mEmail = "";
    SessionManager sessionManager;
    public GoogleApiClient apiClient;
    public GoogleSignInOptions signInOptions;

    @Override
    public void onStart() {
        super.onStart();

        if (mType == "GOO_LOGIN") {
            signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
            apiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
            apiClient.connect();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_settings);

        name = findViewById(R.id.nameTxt);
        btn_logout = findViewById(R.id.btn_logout);
        sessionManager = new SessionManager(this);

        HashMap<String, String> user = sessionManager.getUserDetail();
        mEmail = user.get(sessionManager.EMAIL);
        mType = user.get(sessionManager.TYPE);
        name.setText(mEmail);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mType == "GOO_LOGIN")
                    googleLogout();
                else if (mType == "FACE_LOGIN")
                    disconnectFromFacebook();
                sessionManager.logout();
            }
        });
    }

    private void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        LoginManager.getInstance().logOut();
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    public void googleLogout() {
        if (apiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(apiClient);
            apiClient.disconnect();
            apiClient.connect();
        } else {
            return;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        SoundManager.getInstance().setAudio(false);
        SoundManager.getInstance().playBGMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundManager.getInstance().releaseBGMusic();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SoundManager.getInstance().setAudio(true);
    }
}
