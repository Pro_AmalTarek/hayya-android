package com.solutions.hayya.Views;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.github.andreilisun.swipedismissdialog.OnSwipeDismissListener;
import com.github.andreilisun.swipedismissdialog.SwipeDismissDialog;
import com.github.andreilisun.swipedismissdialog.SwipeDismissDirection;
import com.solutions.hayya.R;
import com.solutions.hayya.Utils.SessionManager;
import com.solutions.hayya.Utils.SoundManager;
import com.solutions.hayya.Views.Adapter.CategoryAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    //region attributes
    private ImageView btn_music, btn_settings, right_arrow, left_arrow, category_title;
    private ViewPager category_pager;
    private CategoryAdapter category_adapter;
    private List<Integer> lstCategories, lstCatTitles, lstCatAudios;
    private Animation mAnimation;
    private MediaPlayer categorySound;
    private SessionManager sessionManager;
    private SwipeDismissDialog swipeDismissDialog;
    private AnimatorSet flipAnim;
    private int categoryPosition = 0;
    //endregion

    //region Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // region Get Variable IDs and Init Classes
        sessionManager = new SessionManager(this);
        btn_settings = findViewById(R.id.settings_btn);
        btn_music = findViewById(R.id.music_btn);
        right_arrow = findViewById(R.id.cat_right);
        left_arrow = findViewById(R.id.cat_left);
        category_pager = findViewById(R.id.category_pager);
        category_title = findViewById(R.id.category_title);
        initCategories();
        initAnimations();
        //endregion

        //region setup music button
        if (!sessionManager.isMute()) {
            btn_music.setImageResource(R.drawable.home_music_off);
            btn_music.setAlpha(0.5f);
        } else {
            btn_music.setImageResource(R.drawable.home_music_on);
            btn_music.setAlpha(1f);
        }
        //endregion

        //region Category_Pager
        category_adapter = new CategoryAdapter(lstCategories, getBaseContext(), mAnimation);
        category_pager.setAdapter(category_adapter);
        category_pager.setCurrentItem(Integer.MAX_VALUE / 2);
        categorySound = MediaPlayer.create(HomeActivity.this, lstCatAudios.get(0));
        categorySound.start();

        category_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                categoryPosition = position % lstCategories.size();
                switchCategoryTitle(categoryPosition);
                categorySound = MediaPlayer.create(HomeActivity.this, R.raw.sound);
                categorySound.start();
                categorySound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (!categorySound.isPlaying()) {
                            categorySound = MediaPlayer.create(HomeActivity.this, lstCatAudios.get(categoryPosition));
                            categorySound.start();
                        }
                    }
                });
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        //endregion

        //region make buttons clickable
        ClickableButton.clickEffect(btn_music);
        ClickableButton.clickEffect(btn_settings);
        ClickableButton.clickEffect(right_arrow);
        ClickableButton.clickEffect(left_arrow);
        //endregion

        //region On Menu Click
        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnSwipeDismissListener swipeDismissListener = new OnSwipeDismissListener() {
                    @Override
                    public void onSwipeDismiss(View view, SwipeDismissDirection direction) {
                        SoundManager.getInstance().setAudio(true);
                        startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
                    }
                };
                View dialog = LayoutInflater.from(getBaseContext()).inflate(R.layout.verify_dialog, null);
                swipeDismissDialog = new SwipeDismissDialog.Builder(HomeActivity.this)
                        .setView(dialog)
                        .setOnSwipeDismissListener(swipeDismissListener)
                        .setFlingVelocity(0.07f)
                        .build()
                        .show();

                ImageView btnCancel = dialog.findViewById(R.id.cancel);
                ClickableButton.clickEffect(btnCancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swipeDismissDialog.dismiss();
                    }
                });
            }
        });
        btn_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.isMute()) {
                    SoundManager.getInstance().setMusic(false);
                    SoundManager.getInstance().releaseBGMusic();
                    btn_music.setImageResource(R.drawable.home_music_off);
                    btn_music.setAlpha(0.5f);
                    sessionManager.editMusic(false);
                } else {
                    SoundManager.getInstance().setMusic(true);
                    SoundManager.getInstance().playBGMusic();
                    btn_music.setImageResource(R.drawable.home_music_on);
                    btn_music.setAlpha(1f);
                    sessionManager.editMusic(true);
                }
            }
        });
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category_pager.arrowScroll(ViewPager.FOCUS_LEFT);
            }
        });
        right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category_pager.arrowScroll(ViewPager.FOCUS_RIGHT);
            }
        });
        //endregion

    }

    private void initAnimations() {
        /*mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.004f);
        mAnimation.setDuration(500);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());*/

        flipAnim = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.flip_in);
        flipAnim.setTarget(category_title); // set the view you want to animate
        flipAnim.setDuration(1000);
        flipAnim.start();

        /*slide_in_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.scale_in);
        slide_out_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.scale_out);*/

    }

    private void initCategories() {
        lstCategories = new ArrayList<>();
        lstCategories.add(R.drawable.home_cat_science);
        lstCategories.add(R.drawable.home_cat_go);
        lstCategories.add(R.drawable.home_cat_lang);
        lstCategories.add(R.drawable.home_cat_math);
        lstCategories.add(R.drawable.home_cat_light);
        lstCategories.add(R.drawable.home_cat_value);
        lstCategories.add(R.drawable.home_cat_world);
        lstCatTitles = new ArrayList<>();
        lstCatTitles.add(R.drawable.home_title_1);
        lstCatTitles.add(R.drawable.home_title_2);
        lstCatTitles.add(R.drawable.home_title_3);
        lstCatTitles.add(R.drawable.home_title_4);
        lstCatTitles.add(R.drawable.home_title_5);
        lstCatTitles.add(R.drawable.home_title_6);
        lstCatTitles.add(R.drawable.home_title_7);
        lstCatAudios = new ArrayList<>();
        lstCatAudios.add(R.raw.sound_1);
        lstCatAudios.add(R.raw.sound_2);
        lstCatAudios.add(R.raw.sound_3);
        lstCatAudios.add(R.raw.sound_4);
        lstCatAudios.add(R.raw.sound_5);
        lstCatAudios.add(R.raw.sound_6);
        lstCatAudios.add(R.raw.sound_7);
    }

    /**
     * make animation flip in and out when change Image Resource
     *
     * @param position
     */
    private void switchCategoryTitle(final int position) {
        flipAnim = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.flip_out);
        flipAnim.setTarget(category_title); // set the view you want to animate
        flipAnim.start();
        flipAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // start anim2 from here
                category_title.setImageResource(lstCatTitles.get(position));
                flipAnim = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.flip_in);
                flipAnim.setTarget(category_title); // set the view you want to animate
                flipAnim.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SoundManager.getInstance().setAudio(false);
        SoundManager.getInstance().playBGMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundManager.getInstance().releaseBGMusic();
       /* if(categorySound != null && categorySound.isPlaying())
            categorySound.stop();*/
    }
    //endregion
}
