package com.solutions.hayya.Views.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solutions.hayya.Model.SubCategory;
import com.solutions.hayya.R;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.SubCategoryHolder> {

    private ArrayList<SubCategory> subCategoriesLst;
    private Context mContext;

    public RecyclerViewAdapter(Context context, ArrayList<SubCategory> categoriesLst) {
        this.subCategoriesLst = categoriesLst;
        this.mContext = context;
    }

    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_sub_category, null);
        SubCategoryHolder subCategoryHolder = new SubCategoryHolder(view);
        return subCategoryHolder;
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder subCategoryHolder, int i) {
        final String sectionName = subCategoriesLst.get(i).getSubCategoryTitle();

        ArrayList sectionItems = subCategoriesLst.get(i).getSubCategoryItems();

        subCategoryHolder.subCategoryTitle.setText(sectionName);

        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, sectionItems);

        subCategoryHolder.recyclerView.setHasFixedSize(true);
        subCategoryHolder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
        subCategoryHolder.recyclerView.setAdapter(itemListDataAdapter);
    }

    @Override
    public int getItemCount() {
        return (null != subCategoriesLst ? subCategoriesLst.size() : 0);
    }

    public class SubCategoryHolder extends RecyclerView.ViewHolder {
        protected TextView subCategoryTitle;
        protected RecyclerView recyclerView;

        public SubCategoryHolder(View view) {
            super(view);

            subCategoryTitle = (TextView) view.findViewById(R.id.subCategoryTitle);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        }
    }

}