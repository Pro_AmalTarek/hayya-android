package com.solutions.hayya.Views.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.solutions.hayya.R;
import com.solutions.hayya.Utils.SoundManager;
import com.solutions.hayya.Views.ClickableButton;
import com.solutions.hayya.Views.ContentActivity;
import com.solutions.hayya.Views.HomeActivity;

import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class CategoryAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    List<Integer> lstCategories;
    boolean flag;
    private int realPos = 0;
    Animation mAnimation;

    public CategoryAdapter(List<Integer> lstGifs, Context context, Animation mAnimation) {
        this.lstCategories = lstGifs;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        flag = true;
        this.mAnimation = mAnimation;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.category , container , false);
        realPos = position % lstCategories.size();
        final GifImageView category =  view.findViewById(R.id.categoryGif);
        category.setBackgroundResource(lstCategories.get(realPos));
        category.setTag(realPos+1);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation zoomAnimation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                v.startAnimation(zoomAnimation);
                SoundManager.getInstance().setAudio(true);
                Intent contentIntent = new Intent(context, ContentActivity.class);
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                contentIntent.putExtra("main_category_id", (int) v.getTag());
                context.startActivity(contentIntent);
            }
        });
        container.addView(view);
        return view;
    }
}
