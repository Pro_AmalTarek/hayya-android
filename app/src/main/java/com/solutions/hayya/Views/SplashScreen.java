package com.solutions.hayya.Views;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DisplayCutout;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

import com.solutions.hayya.R;
import com.solutions.hayya.Utils.SessionManager;

public class SplashScreen extends AppCompatActivity {

    VideoView splashVideo;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(SplashScreen.this,R.color.whiteColor));
        };
        setContentView(R.layout.activity_splash_screen);

        // getSupportActionBar().hide();

        splashVideo = (VideoView) findViewById(R.id.splashVideo);

        Uri Video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.welcome);

        splashVideo.setVideoURI(Video);

        splashVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (isFinishing())
                    return;
                sessionManager = new SessionManager(SplashScreen.this);
                sessionManager.checkLogin();
            }
        });
        splashVideo.start();
    }
}
