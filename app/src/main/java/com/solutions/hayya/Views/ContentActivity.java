package com.solutions.hayya.Views;

import android.app.Dialog;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.solutions.hayya.Business.ContentManager;
import com.solutions.hayya.Business.ServerCallBack;
import com.solutions.hayya.R;
import com.solutions.hayya.Utils.ErrorManager;
import com.solutions.hayya.Utils.SoundManager;
import com.solutions.hayya.Views.Adapter.RecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContentActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private int level_id = 1, main_category_id = 1;
    private ImageView stage1, stage2;
    private ContentManager mContentManager;
    private ErrorManager errorManager;
    private Dialog load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        //TODO : Remove Comment from Original Main Category
        /*Intent homeIntent = getIntent();
        main_category_id = homeIntent.getIntExtra("main_category_id", 1);*/

        stage1 = findViewById(R.id.PreSchool);
        stage2 = findViewById(R.id.Primary);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        errorManager = new ErrorManager(getApplicationContext());
        mContentManager = new ContentManager(errorManager, getApplicationContext());

        //region Create Progress bar
        load = new Dialog(this, android.R.style.Theme_Black);
        View view = LayoutInflater.from(this).inflate(R.layout.custom_loading, null);
        load.requestWindowFeature(Window.FEATURE_NO_TITLE);
        load.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        load.setContentView(view);
        //endregion

        getSubCategories();
        mRecyclerView.setHasFixedSize(true);


        stage2.setAlpha(0.5f);
        stage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stage2.setAlpha(0.5f);
                stage1.setAlpha(1.0f);
                level_id = 1;
                loadAllItems(main_category_id, level_id);
            }
        });

        stage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stage1.setAlpha(0.5f);
                stage2.setAlpha(1.0f);
                level_id = 2;
                loadAllItems(main_category_id, level_id);
            }
        });


    }

    private void getSubCategories() {
        load.show();
        mContentManager.getSubCategories(new ServerCallBack() {
            @Override
            public void onSuccess(int msg) {
                // Show Alert Success ///////
                load.dismiss();
                loadAllItems(main_category_id, level_id);
            }

            @Override
            public void onError() {
                load.dismiss();
                showAlert(errorManager.getMsg(), getApplicationContext().getString(R.string.errorTitle), false);
            }
        });
    }
   /* public void createDummyData() {
        for (int i = 1; i <= 5; i++) {

            SubCategory subCategory = new SubCategory();

            subCategory.setSubCategoryTitle("Section " + i);

            ArrayList<Item> singleItem = new ArrayList<Item>();
            for (int j = 0; j <= 5; j++) {
                singleItem.add(new Item("Item " + j, "URL " + j));
            }

            dm.setAllItemsInSection(singleItem);

            allSampleData.add(dm);

        }
    }*/

    private void loadAllItems(int main_category_id, int level_id) {
        load.show();
        mContentManager.getItems(main_category_id, level_id, new ServerCallBack() {
            @Override
            public void onSuccess(int msg) {
                // Show Alert Success ///////
                load.dismiss();
                mAdapter = new RecyclerViewAdapter(getApplicationContext(), mContentManager.getSubCategories());
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onError() {
                load.dismiss();
                showAlert(errorManager.getMsg(), getApplicationContext().getString(R.string.errorTitle), false);
            }
        });
    }

    //region alert
    public void showAlert(String errorMsg, String titleMsg, boolean status) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContentActivity.this);
        final AlertDialog alert = alertDialogBuilder.create();
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_alert, null);

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView msg = (TextView) view.findViewById(R.id.msg);
        ImageView ok = view.findViewById(R.id.ok);
        ImageView alertImage = view.findViewById(R.id.alertImage);
        title.setText(titleMsg);
        msg.setText(errorMsg);
        if (status)
            alertImage.setImageResource(R.drawable.alert_happy);
        ClickableButton.clickEffect(ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogScale;
        /*InputStream inputStream = getApplicationContext().getResources().openRawResource(R.drawable.alert_background);

        Bitmap b = BitmapFactory.decodeStream(inputStream);
        b.setDensity(Bitmap.DENSITY_NONE);
        Drawable d = new BitmapDrawable(b);*/
        alert.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.alert_background));

        alert.setView(view);
        alert.show();
    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();
        SoundManager.getInstance().setAudio(false);
        SoundManager.getInstance().playBGMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundManager.getInstance().releaseBGMusic();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SoundManager.getInstance().setAudio(true);
    }
}