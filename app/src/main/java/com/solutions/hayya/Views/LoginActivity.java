package com.solutions.hayya.Views;


import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.solutions.hayya.Business.ServerCallBack;
import com.solutions.hayya.Business.UserManager;
import com.solutions.hayya.R;
import com.solutions.hayya.Utils.CheckerManager;
import com.solutions.hayya.Utils.ErrorManager;
import com.solutions.hayya.Utils.SessionManager;
import com.solutions.hayya.Utils.SoundManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //region Attributes
    private ImageView btn_google, btn_facebook, btn_login;
    private LoginButton loginButton;
    EditText edit_email;
    SessionManager sessionManager;
    CallbackManager cbManager;
    String name, email = "", errorMsg = "";
    boolean loginFlag = false;
    public static GoogleApiClient apiClient;
    public static GoogleSignInOptions signInOptions;
    private static final int REQ_Code = 9001;
    CheckerManager checkerManager;
    SignInButton signInButton;
    UserManager userManager;
    ErrorManager errorManager;
    Dialog load;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        // region Get Variable IDs and Init Classes
        sessionManager = new SessionManager(this);
        btn_login = findViewById(R.id.login_btn);
        edit_email = findViewById(R.id.mail_login);
        btn_facebook = findViewById(R.id.facebook_login);
        btn_google = findViewById(R.id.google_login);
        errorManager = new ErrorManager(getApplicationContext());
        checkerManager = new CheckerManager(errorManager);
        userManager = new UserManager(errorManager, getApplicationContext());
        //endregion

        DisplayMetrics metrics = new DisplayMetrics();
        LoginActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Log.i("SettingCardFragment", String.format("%37s%dx%d",
                "Screen resolution: ", metrics.widthPixels, metrics.heightPixels));

        //region make buttons clickable
        ClickableButton.clickEffect(btn_facebook);
        ClickableButton.clickEffect(btn_google);
        ClickableButton.clickEffect(btn_login);
        //endregion

        //region Create Progress bar
        load = new Dialog(this, android.R.style.Theme_Black);
        View view = LayoutInflater.from(this).inflate(R.layout.custom_loading, null);
        load.requestWindowFeature(Window.FEATURE_NO_TITLE);
        load.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        load.setContentView(view);
        //endregion

        // region Facebook Login
        // printKeyHash();

        cbManager = CallbackManager.Factory.create();
        btn_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFlag = false;
                loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
                loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
                loginButton.performClick();
                //printKeyHash();
                loginButton.registerCallback(cbManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.d("Response ", response.toString());
                                //showAlert(response.toString(), getApplicationContext().getString(R.string.errorTitle), false);
                                getData(object);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException error) {
                        // Toast.makeText(LoginActivity.this, "Error to Login Facebook", Toast.LENGTH_SHORT).show();
                        showAlert(error.getMessage(), getApplicationContext().getString(R.string.errorTitle), false);
                    }
                });

            }
        });
        //endregion

        // region Google Login
        signInButton = findViewById(R.id.google_Login_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        apiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFlag = false;
                apiClient.connect();
                onGoogleClick();
            }
        });
        // endregion

        // region On Click Register or Done Listener
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLogin();

            }
        });

        edit_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    performLogin();
                    return true;
                }
                return false;
            }
        });
        //endregion
    }

    /***
     * this is just for get the HAsh Key not else
     */
    private void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.solutions.hayya", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
            showAlert(e.getMessage(), getApplicationContext().getString(R.string.errorTitle), false);
        } catch (NoSuchAlgorithmException e) {
            //e.printStackTrace();
            showAlert(e.getMessage(), getApplicationContext().getString(R.string.errorTitle), false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_Code) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        } else {
            cbManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getData(JSONObject object) {
        try {
            email = object.getString("email");
            name = object.getString("name");
            RegistSocialUser("FACE_LOGIN");

        } catch (JSONException e) {
            //e.printStackTrace();
            LoginManager.getInstance().logOut();
            showAlert(e.getMessage(), getApplicationContext().getString(R.string.errorTitle), false);
        }
    }

    private void onGoogleClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(signInIntent, REQ_Code);
    }


    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            name = account.getDisplayName();
            email = account.getEmail();
            RegistSocialUser("GOO_LOGIN");

        }
    }

    public void intentToHome(String type, String email) {
        SoundManager.getInstance().setMusic(true);
        SoundManager.getInstance().initSounds(getApplicationContext());
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        sessionManager.createSession(type, email);
        intent.putExtra("email", email);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    //region login with Database
    private void RegistSocialUser(final String Login_type) {
        userManager.registSocialUser(name, email, new ServerCallBack() {
            @Override
            public void onSuccess(int msg) {
                // Show Alert Success ///////
                //showAlert(getApplicationContext().getString(R.string.login_success), getApplicationContext().getString(R.string.successTitle));
                intentToHome(Login_type, email); // name handle it
            }

            @Override
            public void onError() {
                showAlert(errorManager.getMsg(), getApplicationContext().getString(R.string.errorTitle), false);
            }
        });
    }


    /**
     * when User Click on Register button or done keyboard
     */
    private void performLogin() {
        load.show();
        email = edit_email.getText().toString().trim();
        errorMsg = checkerManager.confirmEmailInput(email);

        if (errorMsg.equals("")) {

            userManager.Login(email, new ServerCallBack() {
                @Override
                public void onSuccess(int msg) {
                    // Show Alert Success ///////
                    load.dismiss();
                    if (msg == 4) {
                        //showAlert(getApplicationContext().getString(R.string.login_success), getApplicationContext().getString(R.string.successTitle));
                        intentToHome("LOGIN", email);

                    } else {
                        showAlert(getApplicationContext().getString(R.string.register_success), getApplicationContext().getString(R.string.successTitle), true);
                        loginFlag = true;
                    }
                }

                @Override
                public void onError() {
                    load.dismiss();
                    showAlert(errorManager.getMsg(), getApplicationContext().getString(R.string.errorTitle), false);
                }
            });


        } else {
            load.dismiss();
            //Toast.makeText(RegisterActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            showAlert(errorMsg, getApplicationContext().getString(R.string.errorTitle), false);
        }
    }
    //endregion

    //region alert
    public void showAlert(String errorMsg, String titleMsg, boolean status) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        final AlertDialog alert = alertDialogBuilder.create();
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_alert, null);

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView msg = (TextView) view.findViewById(R.id.msg);
        ImageView ok = view.findViewById(R.id.ok);
        ImageView alertImage = view.findViewById(R.id.alertImage);
        title.setText(titleMsg);
        msg.setText(errorMsg);
        if (status)
            alertImage.setImageResource(R.drawable.alert_happy);
        ClickableButton.clickEffect(ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setCancelable(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogScale;
        /*InputStream inputStream = getApplicationContext().getResources().openRawResource(R.drawable.alert_background);

        Bitmap b = BitmapFactory.decodeStream(inputStream);
        b.setDensity(Bitmap.DENSITY_NONE);
        Drawable d = new BitmapDrawable(b);*/
        alert.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.alert_background));

        alert.setView(view);
        alert.show();
    }
    //endregion

    @Override
    protected void onResume() {
        if (loginFlag) {
            performLogin();
            loginFlag = false;
        }
        super.onResume();
    }
}
