package com.solutions.hayya.Model;

import java.util.ArrayList;

public class SubCategory {

    private int id;
    private String subCategoryTitle;
    private ArrayList<Item> subCategoryItems;
    public SubCategory(){

    }

    public SubCategory(int id, String headerTitle) {
        this.id = id;
        this.subCategoryTitle = headerTitle;
        subCategoryItems = new ArrayList<>();
    }

    public String getSubCategoryTitle() {
        return subCategoryTitle;
    }

    public void setSubCategoryTitle(String subCategoryTitleTitle) {
        this.subCategoryTitle = subCategoryTitleTitle;
    }

    public ArrayList<Item> getSubCategoryItems() {
        return subCategoryItems;
    }

    public void addItem(Item item){
        this.subCategoryItems.add(item);
    }

    public void setSubCategoryItems(ArrayList<Item> subCategoryItems) {
        this.subCategoryItems = subCategoryItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
