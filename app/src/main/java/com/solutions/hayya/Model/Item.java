package com.solutions.hayya.Model;

public class Item {

    private int item_id;
    private String item_title;
    private String item_thumb;
    private String item_content;
    private int payment_status;
    private int main_category_id;
    private int sub_category_id;
    private int level_id;

    public Item(int item_id, String item_title, String item_thumb, String item_content, int payment_status, int main_category_id, int sub_category_id, int level_id) {
        this.item_id = item_id;
        this.item_title = item_title;
        this.item_thumb = item_thumb;
        this.item_content = item_content;
        this.payment_status = payment_status;
        this.main_category_id = main_category_id;
        this.sub_category_id = sub_category_id;
        this.level_id = level_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_title() {
        return item_title;
    }

    public void setItem_title(String item_title) {
        this.item_title = item_title;
    }

    public String getItem_thumb() {
        return item_thumb;
    }

    public void setItem_thumb(String item_thumb) {
        this.item_thumb = item_thumb;
    }

    public String getItem_content() {
        return item_content;
    }

    public void setItem_content(String item_content) {
        this.item_content = item_content;
    }

    public int getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(int payment_status) {
        this.payment_status = payment_status;
    }

    public int getMain_category_id() {
        return main_category_id;
    }

    public void setMain_category_id(int main_category_id) {
        this.main_category_id = main_category_id;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public int getLevel_id() {
        return level_id;
    }

    public void setLevel_id(int level_id) {
        this.level_id = level_id;
    }
}
