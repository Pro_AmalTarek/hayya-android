package com.solutions.hayya.Business;

public interface ServerCallBack {
    void onSuccess(int msg);
    void onError();
}
