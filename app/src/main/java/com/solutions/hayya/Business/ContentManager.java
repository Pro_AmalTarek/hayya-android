package com.solutions.hayya.Business;

import android.content.Context;

import com.solutions.hayya.Database.VolleyApiService;
import com.solutions.hayya.Database.VolleyCallback;
import com.solutions.hayya.Model.Item;
import com.solutions.hayya.Model.SubCategory;
import com.solutions.hayya.Model.User;
import com.solutions.hayya.Utils.ErrorManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContentManager {

    private static String URL_ITEMS = "content/items.php";
    private static String URL_SUBCATEGORIES = "content/subCategories.php";

    private ArrayList<SubCategory> subCategories;

    private ErrorManager errorManager;
    private Context context;
    private Item item;

    public ContentManager(ErrorManager errorManager, Context context){
        this.errorManager = errorManager;
        this.context = context;
    }

    public ArrayList<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories() {
        this.subCategories = new ArrayList<>();
    }

    public void getItems(final int main_category_id, final int level_id, final ServerCallBack callBack) {
        clearSubCategoriesItems();
        VolleyApiService volleyApiCAll = new VolleyApiService(context,errorManager);
        volleyApiCAll.Volley_POST(getParams(main_category_id, level_id), URL_ITEMS, new VolleyCallback(){
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                    //String success = jsonObject.getString("message");

                    boolean error = jsonObject.getBoolean("error");
                    if (!error) {
                        JSONArray items = jsonObject.getJSONArray("items");
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject itemObject = items.getJSONObject(i);
                            Item item = new Item(itemObject.getInt("id"),
                                    itemObject.getString("title"),
                                    itemObject.getString("thumb"),
                                    itemObject.getString("content"),
                                    itemObject.getInt("payment_status"),
                                    itemObject.getInt("main_category_id"),
                                    itemObject.getInt("sub_category_id"),
                                    itemObject.getInt("level_id"));
                            getSubCategories().get(item.getSub_category_id()-1).addItem(item);
                        }
                        callBack.onSuccess(0);

                    } else {
                        errorManager.setCode(12);
                        callBack.onError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorManager.setCode(2);
                    callBack.onError();
                }
            }

            @Override
            public void onErrorResponse() {
                callBack.onError();

            }
        });
    }

    private void clearSubCategoriesItems() {
        for(int  i = 0; i< subCategories.size(); i++){
            subCategories.get(i).getSubCategoryItems().clear();
        }
    }

    public void getSubCategories(final ServerCallBack callBack) {
        setSubCategories();
        VolleyApiService volleyApiCAll = new VolleyApiService(context,errorManager);
        volleyApiCAll.Volley_GET(URL_SUBCATEGORIES, new VolleyCallback(){
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));

                    boolean error = jsonObject.getBoolean("error");
                    if (!error) {
                        JSONArray subCategories = jsonObject.getJSONArray("subCategories");
                        for (int i = 0; i < subCategories.length(); i++) {
                            JSONObject subCategoryObject = subCategories.getJSONObject(i);
                            SubCategory subCategory = new SubCategory(subCategoryObject.getInt("id"),
                                    subCategoryObject.getString("name"));
                            getSubCategories().add(subCategory);
                        }
                        callBack.onSuccess(0);

                    } else {
                        errorManager.setCode(12);
                        callBack.onError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorManager.setCode(2);
                    callBack.onError();
                }
            }
            @Override
            public void onErrorResponse() {
                callBack.onError();

            }
        });
    }

    private Map<String,String> getParams(int main_category_id, int level_id)
    {
        // Volley is an HTTP library that makes networking for Android apps easier and most importantly, faster.
        Map<String, String> params = new HashMap<String, String>();
        params.put("main_category_id", "" + main_category_id);
        params.put("level_id", "" + level_id);
        return params;
    }
}
