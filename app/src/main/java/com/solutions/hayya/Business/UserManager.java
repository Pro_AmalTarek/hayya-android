package com.solutions.hayya.Business;

import android.content.Context;

import com.solutions.hayya.Database.VolleyApiService;
import com.solutions.hayya.Database.VolleyCallback;
import com.solutions.hayya.Model.User;
import com.solutions.hayya.Utils.ErrorManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserManager {

    private static String URL_LOGIN = "login/login.php";
    private static String URL_REGIST = "register/register.php";
    private ErrorManager errorManager;
    private Context context;
    private User User;

    public UserManager(ErrorManager errorManager, Context context){
        this.errorManager = errorManager;
        this.context = context;
    }

    public User getUser() {
        return User;
    }

    private void setUser(){
        this.User = new User();
    }

    public void Login(final String email, final ServerCallBack callBack) {

        VolleyApiService volleyApiCAll = new VolleyApiService(context,errorManager);
        volleyApiCAll.Volley_POST(getParams(email), URL_LOGIN, new VolleyCallback(){
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                    //String success = jsonObject.getString("message");

                    boolean error = jsonObject.getBoolean("error");
                    int errorMsg = jsonObject.getInt("message");
                    if (!error) {
                        if(errorMsg == 4){
                            setUser();
                            JSONObject jsonUser = jsonObject.getJSONObject("user");
                            getUser().setId(Integer.parseInt(jsonUser.getString("id").trim()));
                            getUser().setEmail(jsonUser.getString("email").trim()); // email
                        }

                        callBack.onSuccess(errorMsg);

                    } else {
                        errorManager.setCode(errorMsg);
                        callBack.onError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorManager.setCode(2);
                    callBack.onError();
                }
            }

            @Override
            public void onErrorResponse() {
                callBack.onError();

            }

        });

    }

    private Map<String,String> getParams(String email)
    {
        // Volley is an HTTP library that makes networking for Android apps easier and most importantly, faster.
        Map<String, String> params = new HashMap<String, String>();
        params.put("email",email);
        return params;
    }

    public void registSocialUser(final String name, final String email, final ServerCallBack callBack){


        VolleyApiService volleyApiCAll = new VolleyApiService(context,errorManager);
        volleyApiCAll.Volley_POST(getSocialParams(name, email), URL_LOGIN, new VolleyCallback(){
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));

                    int errorMsg = jsonObject.getInt("message");

                    boolean success = jsonObject.getBoolean("error");
                    if (!success) {
                        callBack.onSuccess(errorMsg);

                    } else {
                        errorManager.setCode(2);
                        callBack.onError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorManager.setCode(2);
                    callBack.onError();
                }
            }

            @Override
            public void onErrorResponse() {
                callBack.onError();

            }

        });

    }

    private Map<String,String> getSocialParams(String name, String email)
    {
        // Volley is an HTTP library that makes networking for Android apps easier and most importantly, faster.
        Map<String, String> params = new HashMap<String, String>();
        params.put("email",email);
        params.put("name",name);
        return params;
    }

}
