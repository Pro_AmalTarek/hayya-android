package com.solutions.hayya.Utils;

import android.content.Context;

import com.solutions.hayya.R;
import java.util.HashMap;

public class ErrorManager {

    public Context context;
    HashMap<Integer, String> errors;

    private int errorNum = 0;
    private int code;

    public ErrorManager(Context context) {
        this.context = context;
        setErrors();
    }

    private void setErrors(){
        errors = new HashMap<Integer, String>(){{
            put(1, context.getString(R.string.email_exist_error));
            put(2, context.getString(R.string.json_error));
            put(3, context.getString(R.string.fail_sent_email));
            put(4, context.getString(R.string.not_verified));
            put(5, context.getString(R.string.invalid_mail));
            put(6, context.getString(R.string.invalid_mail));
            put(7, context.getString(R.string.empty_field));
            put(8, context.getString(R.string.timeout_error));
            put(9, context.getString(R.string.auth_failure_error));
            put(10, context.getString(R.string.server_error));
            put(11, context.getString(R.string.network_error));
            put(12, context.getString(R.string.parser_error));

        }};
    }
    public int getErrorNum() {
        return errorNum;
    }

    public void setErrorNum(int errorNum) {
        this.errorNum = errorNum;
    }

    public String getMsg() {
        errorNum--;
        return errors.get(getCode());
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code){
        errorNum++;
        this.code = code;
    }

}