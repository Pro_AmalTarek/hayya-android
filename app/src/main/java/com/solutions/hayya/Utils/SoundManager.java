package com.solutions.hayya.Utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import com.solutions.hayya.R;

import java.util.HashMap;
import java.util.Random;

public class SoundManager {
    private static final SoundManager ourInstance = new SoundManager();

    public static SoundManager getInstance() {
        return ourInstance;
    }

    private SoundManager() {}

    public boolean          audio;
    public boolean          audioSoundFX;
    public boolean          audioMusic;

    private SoundPool       mSoundPool;
    private HashMap<Integer, Integer>       mSoundPoolMap;
    private AudioManager    mAudioManager;
    private Context         mContext;
    private Random          random = new Random();

    private MediaPlayer     menuMusic;
    private MediaPlayer     bGMusic;
    private int             menuMusicCurrentPos = 0;
    private int             bGMusicCurrentPos = 0;



    // set Keep Going Audio is on
    public void setAudio(boolean audioOn){
        if(audioOn){
            audio = true;
        }
        if(!audioOn) {
            audio = false;
        }
    }

    public void setSoundFX(boolean soundFX){
        if(soundFX){
            audioSoundFX = true;
        }
        if(!soundFX) {
            audioSoundFX = false;
        }
    }

    // set Music if mute it or not
    public void setMusic(boolean music){
        if(music){
            audioMusic = true;
        }
        if(!music) {
            audioMusic = false;
        }
    }


    public void initSounds(Context theContext) {

        if (mSoundPool == null){
            mContext = theContext;
            mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
            mSoundPoolMap = new HashMap<Integer, Integer>();
            mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);

            /*mSoundPoolMap.put(1, mSoundPool.load(mContext, R.raw.ufo_laser, 1));
            mSoundPoolMap.put(2, mSoundPool.load(mContext, R.raw.enemy_hunter_one_laser, 1));
            mSoundPoolMap.put(3, mSoundPool.load(mContext, R.raw.enemyhuntertwomissile, 1));
            mSoundPoolMap.put(4, mSoundPool.load(mContext, R.raw.enemy_hunter_three_laser, 1));
            mSoundPoolMap.put(5, mSoundPool.load(mContext, R.raw.enemy_drone, 1));
            mSoundPoolMap.put(6, mSoundPool.load(mContext, R.raw.kamikaze, 1));

           /* mSoundPoolMap.put(14, mSoundPool.load(mContext, R.raw.exploastroidshard, 1));

            mSoundPoolMap.put(11, mSoundPool.load(mContext, R.raw.health, 1));
            mSoundPoolMap.put(12, mSoundPool.load(mContext, R.raw.energy, 1));
            mSoundPoolMap.put(13, mSoundPool.load(mContext, R.raw.shield, 1));

            mSoundPoolMap.put(15, mSoundPool.load(mContext, R.raw.gameover, 1));
            mSoundPoolMap.put(16, mSoundPool.load(mContext, R.raw.gameoverexplo, 1));

            mSoundPoolMap.put(17, mSoundPool.load(mContext, R.raw.menu_beep, 1));
            mSoundPoolMap.put(20, mSoundPool.load(mContext, R.raw.menu_beep, 1));*/

            //menuMusic = MediaPlayer.create(mContext, R.raw.bg_sound);
        }
    }

    public void playSound(int index, boolean pitching, int loop){
        if(audioSoundFX == true && audio == true){

            float randomPitch;

            if (pitching){
                randomPitch = (float)(random.nextInt(3) + 9) / 10;
            }else{
                randomPitch = 1;
            }

            float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mSoundPool.play((Integer) mSoundPoolMap.get(index), streamVolume, streamVolume, 1, loop, randomPitch);
        }
    }


 /*   public void playMenuMusic(){
        if(audioMusic == true && audio == true){
            if (menuMusic == null){
                if(MediaPlayer.create(mContext, R.raw.musicmenu) != null) {
                    menuMusic = MediaPlayer.create(mContext, R.raw.musicmenu);
                    if(menuMusicCurrentPos != 0){
                        menuMusic.seekTo(menuMusicCurrentPos);
                    }
                    menuMusic.start();
                    menuMusic.setVolume(1f , 1f);
                    menuMusic.setLooping(true);
                }
            }
        }
    }*/


    public void releaseMenuMusic(){
        if(menuMusic != null){
            this.menuMusicCurrentPos = menuMusic.getCurrentPosition();
            menuMusic.release();
            menuMusic = null;
        }
    }


    public void playBGMusic(){
        if(audioMusic) {
            if (bGMusic == null) {
                bGMusic = MediaPlayer.create(mContext, R.raw.bg_sound);
                bGMusic.seekTo(bGMusicCurrentPos);
                bGMusic.start();
                bGMusic.setVolume(1f, 1f);
                bGMusic.setLooping(true);
            }
        }
    }


    public void releaseBGMusic(){
        if(bGMusic != null && !audio){
            this.bGMusicCurrentPos = bGMusic.getCurrentPosition();
            bGMusic.release();
            bGMusic = null;
        }
    }
}
