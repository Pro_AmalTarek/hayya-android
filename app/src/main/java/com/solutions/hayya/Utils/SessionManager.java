package com.solutions.hayya.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.solutions.hayya.Views.HomeActivity;
import com.solutions.hayya.Views.LoginActivity;
import com.solutions.hayya.Views.SettingsActivity;
import com.solutions.hayya.Views.SplashScreen;

import java.util.HashMap;

public class SessionManager {
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME="LOGIN";
    private static final String LOGIN="IS_LOGIN";
    public static final String EMAIL="EMAIL";
    private static final String MUSIC="MUSIC";
    public static final String TYPE = "LOGIN_TYPE";

    public SessionManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }
    public void createSession(String type, String email){
        editor.putBoolean(LOGIN,true);
        editor.putString(EMAIL, email);
        editor.putBoolean(MUSIC, true);
        editor.putString(TYPE, type);
        editor.apply();
    }

    public boolean isLogin(){
        return sharedPreferences.getBoolean(LOGIN, false);
    }

    public void checkLogin(){
        Intent intent;
        if(!this.isLogin()){
            intent = new Intent(this.context, LoginActivity.class);
            context.startActivity(intent);
            ((SplashScreen) context).finish();
        }else{
            intent = new Intent(this.context, HomeActivity.class);
            context.startActivity(intent);
            ((SplashScreen) context).finish();
            SoundManager bgMusic = SoundManager.getInstance();
            bgMusic.initSounds(this.context);
            bgMusic.setMusic(sharedPreferences.getBoolean(MUSIC, true));
            bgMusic.playBGMusic();
        }
    }

    public HashMap<String, String> getUserDetail(){
        HashMap<String,String> user = new HashMap<>();
        user.put(EMAIL, sharedPreferences.getString(EMAIL, null));
        user.put(TYPE, sharedPreferences.getString(TYPE, null));
        return user;
    }

    public void editMusic(boolean music){
        editor.putBoolean(MUSIC, music);
        editor.apply();
    }

    public boolean isMute(){
        return sharedPreferences.getBoolean(MUSIC, true);
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(this.context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        ((SettingsActivity) context).finish();
    }
}
