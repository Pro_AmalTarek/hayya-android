package com.solutions.hayya.Utils;

import android.util.Patterns;

import java.util.regex.Pattern;

public class CheckerManager {

    private static final Pattern PHONE_PATTERN =
            Pattern.compile("^[0+]1[1025]\\d{8}$");

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=!])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{1,}" +               //at least 4 characters
                    "$");

    private static final Pattern NUMERIC_PATTERN =
            Pattern.compile("\\d+");

    private ErrorManager errorManager;
    public CheckerManager(ErrorManager errorManager){
        this.errorManager = errorManager;
    }

    private boolean validateEmail(String emailInput) {

        if (emailInput.isEmpty()) {
            errorManager.setCode(7);
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            errorManager.setCode(6);
            return false;
        } else {
            return true;
        }
    }


    private boolean validatePassword(String passwordInput) {

        if (passwordInput.isEmpty()) {
            errorManager.setCode(3);
            return false;
       /* } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            setErrorMsg("Password's too weak");
            return false;*/
        } else {
            return true;
        }
    }


    private boolean validatePhone(String phoneInput) {

        if (phoneInput.isEmpty()) {
            errorManager.setCode(3);
            return false;
        } else if (!PHONE_PATTERN.matcher(phoneInput).matches()) {
            errorManager.setCode(5);
            return false;
        } else {
            return true;
        }
    }



    public String confirmPhoneInput( String phoneInput, String passwordInput) {
        if ( !validatePhone(phoneInput) || !validatePassword(passwordInput)) {
            return errorManager.getMsg();
        }else{
            return "";
        }


    }


    public String confirmEmailInput(String emailInput) {
        if (!validateEmail(emailInput)) {
            return errorManager.getMsg();
        }else{
            return "";
        }

    }

    public boolean validateInput(String input) {
        if (!NUMERIC_PATTERN.matcher(input).matches()) {
            return false;
        } else {
            return true;
        }
    }

}
